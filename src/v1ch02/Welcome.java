package v1ch02;

public class Welcome {
	public static void main(String args[]) {
		String[] greeting = new String[3];
		greeting[0] = "Welcome to Core Java";
		greeting[1] = "by Cay Hosrstmann";
		greeting[2] = "and Gary Cornell";
		
		for(String g : greeting)
			System.out.println(g);
	}
}
