package v1ch04;

import java.util.*;

public class ConstructorTest {
	public static void main(String[] args) {
		Employee4[] staff = new Employee4[3];
		
		staff[0] = new Employee4("Harry", 40000);
		staff[1] = new Employee4(60000);
		staff[2] = new Employee4();
		
		for(Employee4 e : staff) {
			System.out.println("name = " + e.getName() + ", id = " + e.getId() + ", salary = " + e.getSalary());
		}
	}
}


class Employee4 {
	private static int nextId;
	
	private int id;
	private String name = "";  // instance field initialization
	private double salary;
	
	static {
		// static initialization block
		// just run one time
		System.out.println("Hello, Static.");
		Random generator = new Random();
		nextId = generator.nextInt(10000);
	}
	
	{
		// object initialization block
		// run three times
		System.out.println("Hello, Block.");
		id = nextId;
		nextId++;
	}
	
	public Employee4(String n, double s) {
		name = n;
		salary = s;
	}
	
	public Employee4(double s) {
		// calls the Employee4(String, double) constructor
		this("Employee #" + nextId, s);
	}
	
	public Employee4() {
		// name initialized to "" -- see above
		// salary not explicitly set -- initialized to 0
		// id initialized in initialization block
	}
	
	public String getName() {
		return name;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public int getId() {
		return id;
	}
}